FROM alpine as builder

# https://storage.googleapis.com/kubernetes-release/release/stable.txt
ARG K8S_VERSION=v1.30.0
ARG HELM_VERSION=v3.14.4
ARG K9S_VERSION=v0.32.4
ARG KUBEBOX_VERSION=v0.10.0
ARG KUBETAIL_VERSION=1.6.20
ARG TILT_VERSION=0.29.0

RUN apk update && apk add curl
RUN cd /usr/bin && curl -LO "https://storage.googleapis.com/kubernetes-release/release/$K8S_VERSION/bin/linux/amd64/kubectl" && chmod 755 kubectl

RUN cd /tmp && curl -Lo helm.tar.gz "https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz" && \
    tar xzf helm.tar.gz && mv linux-amd64/helm /usr/bin/

RUN curl -Lo k9s.tar.gz https://github.com/derailed/k9s/releases/download/$K9S_VERSION/k9s_Linux_amd64.tar.gz && \
    tar xzf k9s.tar.gz && mv k9s /usr/bin/

RUN curl -Lo kubebox https://github.com/astefanutti/kubebox/releases/download/$KUBEBOX_VERSION/kubebox-linux && \
    chmod +x kubebox && mv kubebox /usr/bin/

RUN curl -Lo kubetail https://raw.githubusercontent.com/johanhaleby/kubetail/$KUBETAIL_VERSION/kubetail && \
    chmod +x kubetail && mv kubetail /usr/bin/

RUN curl -Lo tilt.tar.gz https://github.com/tilt-dev/tilt/releases/download/v$TILT_VERSION/tilt.$TILT_VERSION.linux.x86_64.tar.gz && \
    tar xzf tilt.tar.gz && mv tilt /usr/bin/


FROM alpine

RUN apk update && apk add curl zsh tcpdump ca-certificates bind-tools \
        postgresql-client mysql-client openssl openldap-clients rsync bash

COPY --from=builder /usr/bin/kubectl /usr/bin/
COPY --from=builder /usr/bin/helm /usr/bin/
COPY --from=builder /usr/bin/k9s /usr/bin/
COPY --from=builder /usr/bin/kubebox /usr/bin/
COPY --from=builder /usr/bin/kubetail /usr/bin/
COPY --from=builder /usr/bin/tilt /usr/bin/
